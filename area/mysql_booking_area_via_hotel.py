# -*- coding: utf-8 -*-
import sys
import datetime
import pymysql.cursors


class Mysql:

    def __init__(self, config, db_for):
        self.connect_db(config, db_for)

    def connect_db(self, config, db_for):
        """
        Connect the db through given config file

        :param config configuration file
        """
        db_info = config

        self.db = {}
        for k, v in db_info.items():
            if k == db_for:
                self.db[k] = pymysql.connect(host=v.get("host"), db=v.get("db"), user=v.get("user"),
                                             passwd=v.get("passwd"), charset="utf8")

    # Create table function
    def create_table(self, table_name, db_for, column, primary_key):
        db_key = []

        # Build query string
        for (key, value) in column.items():
            db_key.append(key + ' ' + value)

        sql = "CREATE TABLE IF NOT EXISTS " + table_name \
              + " (" + ', '.join(db_key) \
              + "," + " PRIMARY KEY ( " + ', '.join(primary_key) + " ))"

        self.executeDDL(sql, db_for)

    def select(self, query, query_type, use_db):
        """
        Select the query

        @param    query:        string         fetching sql
        @param    query_type:    string|list    fetching type
        @param    use_db:       string         fetching db
        """
        cursor = self.db[use_db].cursor()
        cursor.execute(query)

        if isinstance(query_type, str):
            if query_type == "all":
                return cursor.fetchall()
            else:
                return cursor.fetchone()
        else:
            try:
                return cursor.fetchmany(query_type)
            except:
                print("Please check the fetchmany option of sql")
                sys.exit()

    def update(self, query, use_db):
        """
        Update the value

        @param    query:         string          fetching sql
        @param    use_db:        string          fetching db
        """
        return self.executeDML(query, use_db)

    def insert(self, query, use_db, table):
        """
        Insert the value

        @param    query:        string         fetching sql
        @param    use_db:       string         fetching db
        @param    table         string          table name
        """
        return self.executeSQL(query, use_db, table)

    def delete(self, query, use_db):
        """
        Delete the value in the given db

        @param    query:        string         fetching sql
        @param    use_db:       string         fetching db
        """
        return self.executeDML(query, use_db)

    def daily_insert(self, json, table, use_db, unique_val_list):
        """
        Insert the value from given json

        @param    json:          dict         data to insert
        @param    table:         string       data to table
        @param    use_db:        string       data to db
        @param    unique_val_list:    list    2 unique value in table
        """
        insert_value = list(json.values())

        placeholders = ', '.join(['%s'] * len(json))

        unique_val_1 = unique_val_list[0]
        unique_val_2 = unique_val_list[1]
        unique_val_3 = unique_val_list[2]
        unique_update = unique_val_1 + " = '" + json[unique_val_1] + "', " + \
                        unique_val_2 + " = '" + json[unique_val_2] + "', " + \
                        unique_val_3 + " = '" + json[unique_val_3] + "'"

        self.sql = "INSERT INTO " + table + "( %s ) VALUES ( %s )" % (",".join(json), placeholders) + \
                   "ON DUPLICATE KEY UPDATE " + unique_update
        self.itemBank = []

        cursor = self.db[use_db].cursor()
        try:
            self.itemBank.append(insert_value)
            # Execute the SQL command
            cursor.executemany(self.sql, self.itemBank)
            # Commit your changes in the database
            self.db[use_db].commit()
            # return last id
            cursor.execute('SELECT id FROM ' + table + ' WHERE ' + unique_update.replace(',', ' AND '))
            last_id = cursor.fetchone()
            return last_id
            # return cursor.execute('select LAST_INSERT_ID()')
        except:
            # Rollback in case there is any error
            self.db[use_db].rollback()
            print("Please check the below data")
            print(self.sql, self.itemBank)
            sys.exit()
        finally:
            self.itemBank = []

    def daily_insert_last(self, use_db):
        cursor = self.db[use_db].cursor()
        try:
            # Execute the SQL command
            cursor.executemany(self.sql, self.itemBank)
            # Commit your changes in the database
            self.db[use_db].commit()
        except:
            # Rollback in case there is any error
            self.db[use_db].rollback()
            sys.exit()

    # Insert or Update
    def executeSQL(self, query, use_db, table):
        placeholders = ', '.join(['%s'] * len(query))
        insert_value = list(query.values())
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = "INSERT INTO " + table + "( %s ) VALUES ( %s )" % (
            ",".join(query), placeholders) + "ON DUPLICATE KEY UPDATE updated_at=%s" % (timestamp)
        cursor = self.db[use_db].cursor()
        try:
            # Execute the SQL command
            cursor.execute(sql, insert_value)
            # Commit your changes in the database
            self.db[use_db].commit()
        except:
            # Rollback in case there is any error
            self.db[use_db].rollback()
            print("Please check the below sql")
            print(query)
            sys.exit()

    # General query
    def executeDML(self, query, use_db):
        self.executionSql(query, use_db)

    def executeDDL(self, query, use_db):
        self.executionSql(query, use_db)

    def executionSql(self, query, use_db):
        cursor = self.db[use_db].cursor()
        try:
            # Execute the SQL command
            cursor.execute(query)
            # Commit your changes in the database
            self.db[use_db].commit()
        except:
            # Rollback in case there is any error
            self.db[use_db].rollback()
            print("Please check the below sql")
            print(query)
            sys.exit()

    def booking_insert(self, json, table, use_db, count):
        """
        Insert the value from given json

        @param    json:          dict         data to insert
        @param    table:         string       data to table
        @param    use_db:        string       data to db
        @param    count:         integer      data count
        """
        insert_value = list(json.values())

        placeholders = ', '.join(['%s'] * len(json))

        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.sql = "INSERT INTO " + table + "( %s ) VALUES ( %s )" % (",".join(json), placeholders) + \
                   "ON DUPLICATE KEY UPDATE ota_hotel_id='%s'" % (json['ota_hotel_id'])
        self.itemBank = []

        cursor = self.db[use_db].cursor()
        try:
            self.itemBank.append(insert_value)
            # Execute the SQL command
            cursor.executemany(self.sql, self.itemBank)
            # Commit your changes in the database
            self.db[use_db].commit()
        except Exception as e:
            # Rollback in case there is any error
            self.db[use_db].rollback()
            print("Please check the below data")
            print(self.sql, e)
            print(json)
            sys.exit()
        finally:
            self.itemBank = []

    def disconnect(self, db):
        self.db[db].close()

    def disconnect_all_db(self):
        for db, db_info in self.db.items():
            self.db[db].close()
