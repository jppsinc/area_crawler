# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AreaItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    ota_id         = scrapy.Field()
    world_code     = scrapy.Field()
    world_name     = scrapy.Field()
    top_code       = scrapy.Field()
    top_name       = scrapy.Field()
    large_name     = scrapy.Field()
    large_code     = scrapy.Field()
    medium_name    = scrapy.Field()
    medium_code    = scrapy.Field()
    small_name     = scrapy.Field()
    small_code     = scrapy.Field()
    detail_name    = scrapy.Field()
    detail_code    = scrapy.Field()
    point_name     = scrapy.Field()
    point_code     = scrapy.Field()
    pass
