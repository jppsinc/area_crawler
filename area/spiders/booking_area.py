# -*- coding: utf-8 -*-

import re
from urllib.parse import urlsplit, urlencode, urlparse

import scrapy
from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class BookingAreaSpider(RedisSpider):
    name = 'booking_area'
    redis_key = 'booking_area'

    custom_settings = {
        "DOWNLOADER_MIDDLEWARES"  : {
            "area.proxy_middlewares.ProxyMiddleware": 1,
        },
        "RANDOMIZE_DOWNLOAD_DELAY": True
    }

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))

        urls = dict()
        num = 0
        for sel in response.css('ul.dest-sitemap__list li.dest-sitemap__list-item:nth-child(4) ul li'):
            for sel_region in sel.css('a'):
                large_url = sel_region.css('::attr(href)').extract_first()
                # pushing the item in list
                urls[num] = large_url
                num += 1

        for i in urls:
            url = response.meta['base_url'] + '/accommodation' + urls[i]
            yield scrapy.Request(url, callback=self.parse_medium_data, meta=response.meta, dont_filter=True)

    def parse_medium_data(self, response):
        m_urls = dict()
        num = 0

        # for not referencing an item
        response.meta['large_code'] = response.css(
            '.sb-searchbox__outer form input[name="dest_id"]::attr(value)').extract_first()
        response.meta['large_name'] = response.css('.sb-searchbox__outer form input[name="ssne"]::attr(value)').extract_first()

        # for sel in response.css('ul.ia_body > li:first-child > ul > li.ia_section_item'):
        for sel in response.css('div[role="section"] ul.bui-carousel__inner:first-child li.bui-carousel__item'):
            medium_url = sel.css('a::attr(href)').extract_first()
            # pushing the item in list
            m_urls[num] = medium_url
            num += 1

        for i in m_urls:
            url = response.meta['base_url'] + m_urls[i]
            yield scrapy.Request(url, callback=self.parse_small_data, meta=response.meta, dont_filter=True)

    def parse_small_data(self, response):
        # for not referencing an item
        response.meta['medium_code'] = response.css(
            '.sb-searchbox__outer form input[name="dest_id"]::attr(value)').extract_first()
        response.meta['medium_name'] = response.css('.sb-searchbox__outer form input[name="ssne"]::attr(value)').extract_first()

        if re.fullmatch('[a-zA-Zō]+', response.meta['medium_name']):
            print('continue')
        else:
            urlmst = '/searchresults.ja.html?{}'
            #url_param = {'city': response.meta['medium_code']}
            url_param = {'dest_type': 'city', 'dest_id': response.meta['medium_code']}
            url = response.meta['base_url'] + urlmst.format(urlencode(url_param))
            #yield scrapy.Request(url, callback=self.parse_detail_data, meta=response.meta, dont_filter=True)
            yield scrapy.Request(url, 
                callback=self.parse_detail_data, 
                meta=response.meta, 
                dont_filter=True,
                headers={
                    "Upgrade-Insecure-Requests":"1",
                    "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36",
                    "sec-ch-ua":'Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92',
                    "sec-ch-ua-mobile":"?0"
                }
            )

    def parse_detail_data(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0
        near_by_hotel = False

        item = AreaItem()
        item['ota_id'] = response.meta['site_id']
        item['top_code'] = 'jp'
        item['top_name'] = '日本'
        item['large_code'] = response.meta['large_code']
        item['large_name'] = response.meta['large_name']
        item['medium_code'] = response.meta['medium_code']
        item['medium_name'] = response.meta['medium_name']

        if 'area' not in response.meta:
            response.meta['area'] = []

        area = []
        for sel in response.css('div#hotellist_inner > div'):
            all_classes = sel.css('::attr(class)').getall()
            # not crawling near by area's hotel - only specific area
            # 当サイトでは北広島市で空室のある宿泊施設はありません！ ヒント：周辺の他の宿泊施設も見てみましょう
            if sel.css('div.bui-alert.bui-alert--info'):
                # print('break')
                near_by_hotel = True
                break
            # no info of the sold out property
            elif 'sold_out_property' in all_classes or sel.css('div.sr_vr_intent--banner'):
                # print('continue')
                continue
            else:
                try:
                    area_info = sel.css('div.sr_card_address_line a::text').extract_first().split(',')
                except:
                    continue
                if len(area_info) == 2:
                    area.append(area_info[0].replace('\n',''))
        area = list(set(area))

        # for not referencing an item
        for sel in response.css('div#filter_district div.filteroptions a'):
            item['small_code'] = sel.css('::attr(data-id)').re_first('di-(\w+)')
            area_unfmt = sel.css('div span::text').extract_first()
            pattern = re.compile(r'\s+')
            item['small_name'] = re.sub(pattern, '', area_unfmt)

            if item['small_name'] not in area:
                continue

            if item['small_name'] in response.meta['area']:
                continue

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)
            # pushing the item in list
            items[number] = item_push
            number += 1

        if len(items) == 0:
            items[number] = item

        yield items

        parsed_url = urlparse(response.url)
        next_page = response.css('ul.bui-pagination__list li a.paging-next::attr(href)').get(default=None)
        if next_page and not near_by_hotel:
            url = "{0.scheme}://{0.netloc}{query}".format(parsed_url, query=str(next_page))
            response.meta['area'] = list(set(response.meta['area'] + area))
            yield scrapy.Request(url, 
                callback=self.parse_detail_data, 
                meta=response.meta, 
                dont_filter=True,
                headers={
                    "Upgrade-Insecure-Requests":"1",
                    "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36",
                    "sec-ch-ua":'Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92',
                    "sec-ch-ua-mobile":"?0"
                }
            )
