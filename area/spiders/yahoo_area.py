import json
import re

from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class YahooAreaSpider(RedisSpider):
    name = 'yahoo_area'
    redis_key = 'yahoo_area'

    def parse(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0

        data = response.body.decode('utf-8')

        BT_PREF_AREA_MST = re.findall(' = (\S+)', data)[0]
        BT_L_AREA_MST = re.findall(' = (\S+)', data)[1]
        BT_M_AREA_MST = re.findall(' = (\S+)', data)[2]

        pref_area_json = json.loads(BT_PREF_AREA_MST)
        large_area_json = json.loads(BT_L_AREA_MST)
        mid_area_json = json.loads(BT_M_AREA_MST)

        for mid in mid_area_json:
            item = AreaItem()
            item['ota_id'] = response.meta['site_id']

            large = mid_area_json[mid]['parentcode']
            pref = large_area_json[large]['parentcode']

            item['small_code'] = mid
            item['small_name'] = mid_area_json[mid]['name']

            item['medium_code'] = large
            item['medium_name'] = large_area_json[large]['name']

            item['large_code'] = pref
            item['large_name'] = pref_area_json[pref]['name']

            items[number] = item
            number += 1

        yield items
