# -*- coding: utf-8 -*-

from urllib.parse import urlsplit, urlencode, parse_qs, urlparse

import scrapy
from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class BookingAreaViaHotelSpider(RedisSpider):
    name = 'booking_area_via_hotel'
    redis_key = 'booking_area_via_hotel'

    custom_settings = {
        "DOWNLOADER_MIDDLEWARES"  : {
            "area.header_middleware.HeaderMiddleware": 1,
        },
        "REDIRECT_MAX_TIMES": 100,
        "HTTPCACHE_ENABLED": True
    }

    def parse(self, response):
        url_split = urlsplit(response.url)
        if 'https' not in url_split.scheme:
            url = "https://{0.netloc}/{0.path}".format(url_split)
            yield scrapy.FormRequest(url, meta=response.meta, callback=self.parse)
        elif '/jp/' in url_split.path:
            items = dict()
            item = {}
            item['ota_id'] = str(response.meta['site_id'])

            area_name = response.xpath("//script[contains(.,'window.utag_data =')]/text()")
            destination_type = area_name.re_first(r"dest_type: '(\S+)'")

            hotel_id = None
            if destination_type == 'hotel':
                hotel_id = area_name.re_first(r"dest_id: '(\S+)'")

            if response.meta['hotel_id'] == hotel_id:
                item['ota_hotel_id'] = hotel_id
                item['hotel_name'] = ''.join(response.css('h2.hp__hotel-name::text').getall()).strip()
                item['top_name'] = area_name.re_first(r"country_name: '(\S+)'")
                item['large_name'] = area_name.re_first(r"region_name: '(\S+)'")
                item['medium_name'] = area_name.re_first(r"city_name: '(\S+)'")
                item['small_name'] = area_name.re_first(r"district_name: '(\S+)'")

                dest_info = response.xpath("//script[contains(.,'avoidingXSSviaLocationHash')]/text()")
                dest_type = dest_info.re_first(r"params.context_dest_type = '(\S+)'")
                dest_id = dest_info.re_first(r"params.context_dest_id = '(\S+)'")
                if dest_type == 'city':
                    item['medium_code'] = dest_id
                if item['small_name'] is not '-1':
                    item['small_code'] = response.xpath("//script[contains(.,'markersOnMapURL:')]/text()").re_first(r"districtId: '(\S+)'")

                # for not referencing an item
                items[0] = item

                yield items
