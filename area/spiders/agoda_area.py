import re
from urllib.parse import urlsplit

from area.items.area_item import AreaItem
from bs4 import BeautifulSoup
from scrapy.http.request.form import FormRequest
from scrapy_redis.spiders import RedisSpider


class AgodaAreaSpider(RedisSpider):
    name = 'agoda_area'
    redis_key = 'agoda_area'

    custom_settings = {
        "DOWNLOADER_MIDDLEWARES"  : {
            "area.header_middleware.HeaderMiddleware": 1,
        },
        "RANDOMIZE_DOWNLOAD_DELAY": True
    }

    start_urls = ['https://www.agoda.com/ja-jp/country/japan.html']

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))

        item = AreaItem()
        item['ota_id'] = response.meta['site_id']
        item['top_name'] = re.findall(r'country/(\w+)', response.url)[0]
        item['top_code'] = country_code = response.xpath(
            "//script[contains(.,'geoObjectId')]/text()").re_first('geoObjectId = (\d+)')
        page_code = response.xpath(
            "//script[contains(.,'pageTypeId')]/text()").re_first('pageTypeId = "(\d+)"')

        url = "{base_url}/api/ja-jp/GeoApi/AllStates/{page}/{country}/0".format(base_url=response.meta['base_url'],
                                                                                page=page_code, country=country_code)
        response.meta['item'] = item
        yield FormRequest(url, callback=self.parse_large_data, meta=response.meta)

    def parse_large_data(self, response):
        item = response.meta['item']

        urls = dict()
        num = 0
        soup = BeautifulSoup(response.body, features="lxml")
        for a in soup.find_all('placeviewmodel'):
            large_url = a._x003c_url_x003e_k__backingfield.string
            item['large_name'] = a._x003c_name_x003e_k__backingfield.string

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)
            # pushing the item in list
            urls[num] = {'url': large_url, 'item': item_push}
            num += 1

        for i in urls:
            response.meta['item'] = urls[i]['item']
            url = response.meta['base_url'] + urls[i]['url']
            yield FormRequest(url, callback=self.parse_large_url, meta=response.meta)

    def parse_large_url(self, response):
        item = response.meta['item']

        item['large_code'] = region_code = response.xpath(
            "//script[contains(.,'geoObjectId')]/text()").re_first('geoObjectId = (\d+)')
        page_code = response.xpath(
            "//script[contains(.,'pageTypeId')]/text()").re_first('pageTypeId = "(\d+)"')

        url = "{base_url}/api/ja-jp/GeoApi/NeighborHoods/{page}/{region}/0".format(base_url=response.meta['base_url'],
                                                                                   page=page_code, region=region_code)
        yield FormRequest(url, callback=self.parse_medium_data, meta=response.meta)

    def parse_medium_data(self, response):
        item = response.meta['item']

        m_urls = dict()
        num = 0
        soup = BeautifulSoup(response.body, features="lxml")
        for a in soup.find_all('placeviewmodel'):
            medium_url = a._x003c_url_x003e_k__backingfield.string
            item['medium_name'] = a._x003c_name_x003e_k__backingfield.string

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)
            # pushing the item in list
            m_urls[num] = {'url': medium_url, 'item': item_push}
            num += 1

        for i in m_urls:
            response.meta['item'] = m_urls[i]['item']
            url = response.meta['base_url'] + m_urls[i]['url']
            yield FormRequest(url, callback=self.parse_medium_url, meta=response.meta)

    def parse_medium_url(self, response):
        item = response.meta['item']

        item['medium_code'] = city_code = response.xpath(
            "//script[contains(.,'geoObjectId')]/text()").re_first('geoObjectId = (\d+)')
        page_code = response.xpath(
            "//script[contains(.,'pageTypeId')]/text()").re_first('pageTypeId = "(\d+)"')

        url = "{base_url}/api/ja-jp/GeoApi/NeighborHoods/{page}/{city}/0".format(base_url=response.meta['base_url'],
                                                                                 page=page_code, city=city_code)
        yield FormRequest(url, callback=self.parse_small_data, meta=response.meta)

    def parse_small_data(self, response):
        item = response.meta['item']

        s_urls = dict()
        num = 0
        soup = BeautifulSoup(response.body, features="lxml")
        for a in soup.find_all('placeviewmodel'):
            small_url = a._x003c_url_x003e_k__backingfield.string
            item['small_name'] = a._x003c_name_x003e_k__backingfield.string

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)
            # pushing the item in list
            s_urls[num] = {'url': small_url, 'item': item_push}
            num += 1

        for i in s_urls:
            response.meta['item'] = s_urls[i]['item']
            url = response.meta['base_url'] + s_urls[i]['url']
            yield FormRequest(url, callback=self.parse_small_url, meta=response.meta)

    def parse_small_url(self, response):
        # save all item to an dict : items
        items = dict()

        item = response.meta['item']
        item['small_code'] = response.xpath(
            "//script[contains(.,'geoObjectId')]/text()").re_first('geoObjectId = (\d+)')
        # for not referencing an item
        item_push = AreaItem()
        item_push.update(item)
        # pushing the item in list
        items[0] = item_push

        # crawl
        yield items
