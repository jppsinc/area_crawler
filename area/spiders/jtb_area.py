import re
from urllib.parse import urlsplit

import scrapy
from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class JtbAreaSpider(RedisSpider):
    name = 'jtb_area'
    redis_key = 'jtb_area'

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))

        urls = dict()
        num = 0
        for sel in response.css('div.domhotel-modal-area__map ul'):
            item = AreaItem()
            item['ota_id'] = response.meta['site_id']

            for sel_pref in sel.css('li'):
                url = sel_pref.css('a::attr(href)').extract_first()
                item['large_code'] = re.findall('(\d+)', url)[0]
                item['large_name'] = sel_pref.css('a::text').extract_first()

                # for not referencing an item
                item_push = AreaItem()
                item_push.update(item)
                # pushing the item in list
                urls[num] = {'url': url, 'item': item_push}
                num += 1

        for i in urls:
            response.meta['item'] = urls[i]['item']
            url = response.meta['base_url'] + urls[i]['url']
            yield scrapy.Request(url, callback=self.parse_small_data, meta=response.meta, dont_filter=True)

    def parse_small_data(self, response):
        s_urls = dict()
        num = 0

        item = response.meta['item']
        for sel in response.css('map[name=Map_all] a'):
            url = sel.css('::attr(href)').extract_first()
            item['medium_code'] = re.findall(r'(\d+)[^?]+', url)[0]
            item['medium_name'] = sel.css('::text').extract_first()

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)
            # pushing the item in list
            s_urls[num] = {'url': url, 'item': item_push}
            num += 1

        for i in s_urls:
            response.meta['item'] = s_urls[i]['item']
            url = response.meta['base_url'] + s_urls[i]['url']
            yield scrapy.Request(url, callback=self.parse_detail_data, meta=response.meta, dont_filter=True)

    def parse_detail_data(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0

        item = response.meta['item']
        for sel in response.css('section.spk_js_click-filter-area ul li'):
            item['small_code'] = sel.css('div input::attr(value)').extract_first()
            item['small_name'] = sel.css('div label::text').extract_first()

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)

            items[number] = item_push
            number += 1

        yield items
