import re
from urllib.parse import urlsplit

import scrapy
from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class JalanAreaSpider(RedisSpider):
    name = 'jalan_area'
    redis_key = 'jalan_area'

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))

        urls = dict()
        num = 0
        for sel in response.css('div#search-by-area div.map-alternative dl'):
            item = AreaItem()
            item['ota_id'] = response.meta['site_id']
            item['large_name'] = sel.css('dt::text').extract_first()
            item['large_code'] = sel.css('dt::text').extract_first()

            for sel_li in sel.css('dd'):
                item['medium_name'] = sel_li.css('a::text').extract_first()
                url = sel_li.css('a::attr(href)').extract_first()

                # for not referencing an item
                item_push = AreaItem()
                item_push.update(item)
                # pushing the item in list
                urls[num] = {'url': url, 'item': item_push}
                num += 1

        for i in urls:
            response.meta['item'] = urls[i]['item']
            url = response.meta['base_url'] + urls[i]['url']
            yield scrapy.Request(url, callback=self.parse_small_data, meta=response.meta, dont_filter=True)

    def parse_small_data(self, response):
        s_urls = dict()
        item = response.meta['item']

        item['medium_code'] = medium_code = response.css('input#prefMapId::attr(value)').extract_first()
        for count, sel_ken in enumerate(response.css('div.map-right-inner ul li.s12_66')):
            item['small_code'] = small_code = sel_ken.css('a::attr(onclick)').re_first(r'\d+')
            item['small_name'] = sel_ken.css('a::text').extract_first()
            url = '/' + str(medium_code) + '/LRG_' + str(small_code) + '/'
            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)
            # pushing the item in list
            s_urls[count] = {'url': url, 'item': item_push}

        for i in s_urls:
            response.meta['item'] = s_urls[i]['item']
            url = response.meta['base_url'] + s_urls[i]['url']
            yield scrapy.Request(url, callback=self.parse_detail_data, meta=response.meta, dont_filter=True)

    def parse_detail_data(self, response):
        # save all item to an dict : items
        items = dict()

        item = response.meta['item']
        part_of_small_code = item['small_code'][0:4]
        for count, sel_area in enumerate(response.css('div#dyn_area_d_list .dyn_area_d_list_itm div:not(.close_btn)')):
            detail_code = sel_area.css('a::attr(onclick)').re_first(r'\d+')
            if part_of_small_code in detail_code:
                item['detail_code'] = detail_code
                item['detail_name'] = sel_area.css('div a::text').extract_first()
                # for not referencing an item
                item_push = AreaItem()
                item_push.update(item)
                # pushing the item in list
                items[count] = item_push

        yield items
