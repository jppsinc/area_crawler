import re
from datetime import datetime, timedelta
from urllib.parse import urlsplit

import scrapy
from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class ExpediaAreaSpider(RedisSpider):
    name = 'expedia_area'
    redis_key = 'expedia_area'

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))
        # pagination
        pages = 4
        response.meta['top_id'] = '89'
        for i in range(1, pages):
            if i == 1:
                url = response.url
            else:
                data_offset = 'p' + str(i)
                url = response.meta['base_url'] + '/Destinations-In-.d' + response.meta['top_id'] + '-' + \
                      str(data_offset) + '.Hotel-Destinations'
            yield scrapy.Request(url, callback=self.parse_page, meta=response.meta, dont_filter=True)

    def parse_page(self, response):
        check_in = datetime.now() + timedelta(days=int(90))
        start_date = check_in.now().strftime('%Y/%m/%d')
        check_out = datetime.now() + timedelta(days=int(91))
        end_date = check_out.now().strftime('%Y/%m/%d')

        for sel in response.css('ul#links-container-links-1 li'):
            item = AreaItem()
            item['ota_id'] = response.meta['site_id']

            item['top_id'] = response.meta['top_id']
            item['top_name'] = '日本'

            item['large_name'] = (sel.css('::attr(data-name)').extract_first()).replace('のホテル', '')
            large_href = (sel.css('a::attr(href)').extract_first())
            item['large_code'] = re.findall('(\d+)', large_href)

            response.meta['item'] = item
            url = response.meta['base_url'] + '/Hotel-Search?startDate=' + start_date + '&endDate=' + end_date + \
                  '&regionId=' + item['large_code']

            yield scrapy.Request(url, callback=self.parse_detail_data, meta=response.meta, dont_filter=True)

    def parse_detail_data(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0

        item = response.meta['item']
        for sel in response.css('ul.cityLinkContainer li'):
            item['medium_code'] = sel.css('label input.hidecity::attr(value)').extract_first()
            item['medium_name'] = sel.css('span.cityName::text').extract_first()

            # for not referencing an item
            item_push = AreaItem()
            item_push.update(item)

            # pushing the item in list
            items[number] = item_push
            number += 1

        yield items
