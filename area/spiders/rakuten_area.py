from urllib.request import urlopen
from xml.etree.ElementTree import parse

from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class RakutenAreaSpider(RedisSpider):
    name = 'rakuten_area'
    redis_key = 'rakuten_area'

    def parse(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0

        req_url = urlopen(response.url)

        temp_xml = parse(req_url)

        for data in temp_xml.iterfind('areaClasses/largeClasses/largeClass'):
            item = AreaItem()
            item['ota_id'] = response.meta['site_id']
            # country
            if set(data.findtext('largeClassCode')):
                item['top_code'] = data.findtext('largeClassCode')
                item['top_name'] = data.findtext('largeClassName')
                # prefecture
                for mid in data.iterfind('middleClasses/middleClass'):
                    item['large_code'] = mid.findtext('middleClassCode')
                    item['large_name'] = mid.findtext('middleClassName')
                    # city
                    for sm in mid.iterfind('smallClasses/smallClass'):
                        item['medium_code'] = sm.findtext('smallClassCode')
                        item['medium_name'] = sm.findtext('smallClassName')
                        # ward
                        if set(sm.iterfind('detailClasses/detailClass')):
                            for detail in sm.iterfind('detailClasses/detailClass'):
                                item['small_code'] = detail.findtext('detailClassCode')
                                item['small_name'] = detail.findtext('detailClassName')
                                # for not referencing an item
                                item_push = AreaItem()
                                item_push.update(item)
                                # pushing the item in list
                                items[number] = item_push
                                number += 1
                        else:
                            item['small_code'] = None
                            item['small_name'] = None
                            # for not referencing an item
                            item_push = AreaItem()
                            item_push.update(item)
                            # pushing the item in list
                            items[number] = item_push
                            number += 1
            yield items
