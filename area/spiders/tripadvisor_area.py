import re
from urllib.parse import urlsplit

import scrapy
from area.items.area_item import AreaItem
from scrapy_redis.spiders import RedisSpider


class TripadvisorAreaSpider(RedisSpider):
    name = 'tripadvisor_area'
    redis_key = 'tripadvisor_area'

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))
        data_offset = 0

        urls = dict()
        num = 0
        # pagination
        page_number = response.css(
            'div.pagination_wrapper div.pageNumbers a:last-child::attr(data-page-number)').extract_first()
        total_page = int(page_number) + 1
        for i in range(1, total_page):
            if i == 1:
                url = response.url

                for sel in response.css('div.leaf_geo_list_wrapper div.geo_wrap'):
                    detail_url = sel.css('a::attr(href)').extract_first()
                    detail_url = response.meta['base_url'] + detail_url

                    yield scrapy.Request(detail_url, callback=self.parse_detail_data, meta=response.meta,
                                         dont_filter=True)
            else:
                data_offset += 20
                url = response.meta['base_url'] + '/Hotels-g' + str(response.meta['level_code']) + '-oa' + str(
                    data_offset) + '-Japan-Hotels.html'
                urls[num] = url
                num += 1

        for url in urls:
            yield scrapy.Request(urls[url], callback=self.parse_page, meta=response.meta, dont_filter=True)

    def parse_page(self, response):
        d_urls = dict()
        num = 0

        for sel in response.css('ul.geoList li'):
            detail_url = sel.css('a::attr(href)').extract_first()
            detail_url = response.meta['base_url'] + detail_url

            d_urls[num] = detail_url
            num += 1

        for url in d_urls:
            yield scrapy.Request(d_urls[url], callback=self.parse_detail_data, meta=response.meta, dont_filter=True)

    def parse_detail_data(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0

        item = AreaItem()
        item['ota_id'] = response.meta['site_id']

        top = response.css('ul.breadcrumbs li:nth-child(2) a::attr(href)').extract_first()
        item['top_code'] = re.sub("[^0-9]", "", top)
        item['top_name'] = response.css('ul.breadcrumbs li:nth-child(2) a span::text').extract_first()

        large = response.css('ul.breadcrumbs li:nth-child(3) a::attr(href)').extract_first()
        item['large_code'] = re.sub("[^0-9]", "", large)
        item['large_name'] = response.css('ul.breadcrumbs li:nth-child(3) a span::text').extract_first()

        medium = response.css('ul.breadcrumbs li:nth-child(4) a::attr(href)').extract_first()
        item['medium_code'] = re.sub("[^0-9]", "", medium)
        item['medium_name'] = response.css('ul.breadcrumbs li:nth-child(4) a span::text').extract_first()

        small = response.css('ul.breadcrumbs li:nth-child(5) a::attr(href)').extract_first()
        if small is not None:
            item['small_code'] = re.sub("[^0-9]", "", small)
            item['small_name'] = response.css('ul.breadcrumbs li:nth-child(5) a span::text').extract_first()

        detail = response.css('ul.breadcrumbs li:nth-child(6) a::attr(href)').extract_first()
        if detail is not None:
            item['detail_code'] = re.sub("[^0-9]", "", detail)
            item['detail_name'] = response.css('ul.breadcrumbs li:nth-child(6) a span::text').extract_first()

        point = response.css('ul.breadcrumbs li:nth-child(7) a::attr(href)').extract_first()
        if point is not None:
            item['point_code'] = re.sub("[^0-9]", "", point)
            item['point_name'] = response.css('ul.breadcrumbs li:nth-child(7) a span::text').extract_first()

        items[number] = item
        number += 1

        yield items
