from scrapy_redis.spiders import RedisSpider
from area.items.area_item import AreaItem
import scrapy
import re

class IkyucazAreaSpider(RedisSpider):
    name = 'ikyucaz_area'
    redis_key = 'ikyucaz_area'

    def parse(self, response):
        # save all item to an dict : items
        items = dict()
        number = 0

        large_items = {}
        for sel in response.css('div.areaBox div.largeAreaItem'):
            area_code = sel.css('input.largeArea::attr(value)').extract_first()
            area_name = sel.css('label.largeAreaName::attr(data-area-name)').extract_first()
            large_items[area_code] = area_name

        for sel in response.css('div.smallAreaListBox div.smallAreaItem'):

            item = AreaItem()
            item['ota_id'] = response.meta['site_id']

            #data_parent = sel.css('input::attr(data-parent)').extract_first()
            large_code_tmp = sel.css('input.smallArea::attr(name)').extract_first()
            large_code = re.sub("[^0-9]", "", large_code_tmp)
            medium_code = sel.css('input.smallArea::attr(value)').extract_first()
            medium_name = sel.css('label.areaName::attr(data-area-name)').extract_first()

            if large_code == medium_code:
                continue
            else:
                item['large_code'] = large_code
                item['large_name'] = large_items[large_code]
                item['medium_code'] = medium_code
                item['medium_name'] = medium_name

                # for not referencing an item
                item_push = AreaItem()
                item_push.update(item)
                # pushing the item in list                
                items[number] = item_push                
                number += 1
                
        yield items
