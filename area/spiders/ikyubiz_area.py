from scrapy_redis.spiders import RedisSpider
from area.items.area_item import AreaItem
from urllib.parse import urlsplit
import scrapy
import re
import json

class IkyubizAreaSpider(RedisSpider):
    name = 'ikyubiz_area'
    redis_key = 'ikyubiz_area'

    def parse(self, response):
        response.meta['base_url'] = "{0.scheme}://{0.netloc}".format(urlsplit(response.url))

        for sel in response.xpath("//script/text()"):
            if 'window.__NUXT__' in sel.extract():

                large_items = dict()
                items = dict()
                large_cnt = 0
                cnt = 0

                area_tmp = sel.get()
                area_tmp = area_tmp.split(',"Area:')

                for area in area_tmp:
                    area_code = re.findall('{id:"(\d+)",', area)
                    area_name = re.findall('name:"(.*)",urlPrefix', area)

                    if len(area_code) == 0 or len(area_name) == 0:
                        continue

                    #large, mediumまでで、これより詳細のエリアは取得しない
                    if len(area_code[0]) > 6 or '00000' in area_code[0]:
                        # print(area_code, area_name)
                        # print('continue')
                        continue

                    item = AreaItem()
                    item['ota_id'] = response.meta['site_id']

                    if '0000' in area_code[0]:
                        item['large_code'] = area_code[0]
                        item['large_name'] = area_name[0]

                        large_items[large_cnt] = item
                        large_cnt += 1
                    else:

                        item['large_code'] = large_items[large_cnt-1]['large_code']
                        item['large_name'] = large_items[large_cnt-1]['large_name']
                        item['medium_code'] = area_code[0]
                        item['medium_name'] = area_name[0]
                        
                        items[cnt] = item
                        cnt += 1

                yield items
                break

    #     large_items = {}
    #     for sel in response.css('div[data-v-58f82458] > a'):

    #         area_name = sel.css('::text').extract_first()
    #         area_tmp  = sel.css('::attr(href)').extract_first()
    #         area_code = re.sub("[^0-9]", "", area_tmp)

    #         if len(area_tmp.split('/')) > 4 or '0000' not in area_code:
    #             continue

    #         item = AreaItem()
    #         item['ota_id'] = response.meta['site_id']
    #         item['large_name'] = area_name
    #         item['large_code'] = area_code

    #         response.meta['item'] = item
    #         url = response.meta['base_url'] + area_tmp

    #         yield scrapy.Request(url, callback=self.parse_medium_data, meta=response.meta, dont_filter=True)

    # def parse_medium_data(self, response):
    #     # save all item to an dict : items
    #     items = dict()
    #     item = response.meta['item']


    #     for sel in response.css('section.p-thema-area'):
    #         if 'エリアから探す' in sel.css('div.p-thema-title h2::text').extract_first():
    #             for count,area in enumerate(sel.css('li')):
    #                 area_tmp = area.css('a::attr(href)').extract_first()
    #                 item['medium_code'] = re.sub("[^0-9]", "", area_tmp)
    #                 item['medium_name'] = area.css('span::text').extract_first()

    #                 # for not referencing an item
    #                 item_push = AreaItem()
    #                 item_push.update(item)
    #                 items[count] = item_push
    #             break

    #     yield items
