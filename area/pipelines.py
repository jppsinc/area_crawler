# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from area.common.configuration import Configuration
from area.mysql import Mysql


class AreaPipeline(object):
    table_name = ''

    def __init__(self):
        self.db_for = 'area'
        self.table_area_ota_top = 'ota_area_top'
        self.table_area_ota_large = 'ota_area_large'
        self.table_area_ota_medium = 'ota_area_medium'
        self.table_area_ota_small = 'ota_area_small'
        self.table_area_ota_detail = 'ota_area_detail'
        self.table_area_ota_point = 'ota_area_point'
        self.config = Configuration.get_config_file()

        self.insert_count = 1
        self.current_ota = ''

    def open_spider(self, spider):
        self.current_ota = spider.name.split('_')[0]

        # initialize db
        self.db = Mysql(self.config, self.db_for)

    def process_item(self, items, spider):
        # Use update or insert function
        for v in items:
            item = items[v]

            if item != {}:
                world_query = 'SELECT id FROM ota_area_world WHERE ota_id=' + item['ota_id']
                world_id = self.db.select(world_query, 'one', self.db_for)

                top_area = self.ota_top_code(self.current_ota, item)
                top_value = {'top_code': top_area['top_code'], 'top_name': top_area['top_name'],
                             'ota_id'  : item['ota_id']}
                if world_id is not None:
                    top_value.update({'world_id': world_id[0]})
                top_unique = ['top_code', 'top_name', 'ota_id']
                top_id = self.db.daily_insert(top_value, self.table_area_ota_top, self.db_for, top_unique)

                large_value = {'top_id'    : top_id, 'large_code': item['large_code'],
                               'large_name': item['large_name'], 'ota_id': item['ota_id']}
                large_unique = ['large_code', 'large_name', 'ota_id']
                large_id = self.db.daily_insert(large_value, self.table_area_ota_large, self.db_for, large_unique)

                medium_value = {'top_id'     : top_id, 'large_id': large_id, 'medium_code': item['medium_code'],
                                'medium_name': item['medium_name'], 'ota_id': item['ota_id']}
                medium_unique = ['medium_code', 'medium_name', 'ota_id']
                medium_id = self.db.daily_insert(medium_value, self.table_area_ota_medium, self.db_for, medium_unique)

                if item.get('small_code') is not None:
                    small_value = {'top_id'    : top_id, 'medium_id': medium_id, 'small_code': item['small_code'],
                                   'small_name': item['small_name'], 'ota_id': item['ota_id']}
                    small_unique = ['small_code', 'small_name', 'ota_id']
                    small_id = self.db.daily_insert(small_value, self.table_area_ota_small, self.db_for, small_unique)

                    if item.get('detail_code') is not None:
                        detail_value = {'top_id'     : top_id, 'small_id': small_id, 'detail_code': item['detail_code'],
                                        'detail_name': item['detail_name'], 'ota_id': item['ota_id']}
                        detail_unique = ['detail_code', 'detail_name', 'ota_id']
                        detail_id = self.db.daily_insert(detail_value, self.table_area_ota_detail, self.db_for,
                                                         detail_unique)

                        if item.get('point_code') is not None:
                            point_value = {'top_id'    : top_id, 'detail_id': detail_id,
                                           'point_code': item['point_code'],
                                           'point_name': item['point_name'], 'ota_id': item['ota_id']}
                            point_unique = ['point_code', 'point_name', 'ota_id']
                            point_id = self.db.daily_insert(point_value, self.table_area_ota_point, self.db_for,
                                                            point_unique)

            self.insert_count += 1
        return items

    @staticmethod
    def ota_top_code(ota, item):
        area_top = {'rakuten': {'top_code': 'japan', 'top_name': '日本'},
                    'jalan'  : {'top_code': 'Pj_japan', 'top_name': '日本'},
                    'ikyu'   : {'top_code': 'Pi_japan', 'top_name': '日本'},
                    'ikyubiz': {'top_code': 'Pi_japan', 'top_name': '日本'},
                    'ikyucaz': {'top_code': 'Pi_japan', 'top_name': '日本'},
                    'jtb'    : {'top_code': 'Pjtb_japan', 'top_name': '日本'},
                    'yahoo'  : {'top_code': 'Py_japan', 'top_name': '日本'},
                    'booking': {'top_code': 'jp', 'top_name': '日本'}
                    }

        if ota == 'tripadvisor':
            area_top.update({'tripadvisor': {'top_code': item['top_code'], 'top_name': item['top_name']}})
        if ota == 'agoda':
            area_top.update({'agoda': {'top_code': item['top_code'], 'top_name': item['top_name']}})
        if ota == 'rurubu':
            area_top.update({'rurubu': {'top_code': item['top_code'], 'top_name': item['top_name']}})
        if ota == 'expedia':
            area_top.update({'expedia': {'top_code': item['top_code'], 'top_name': item['top_name']}})

        return area_top[ota]
