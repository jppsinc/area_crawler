# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import json

import requests
from area.common.configuration import Configuration
from area.mysql import Mysql


class AreaPipeline(object):
    table_name = ''

    def __init__(self):
        self.db_for = 'area'
        self.table_area_ota_top = 'ota_area_top'
        self.table_area_ota_large = 'ota_area_large'
        self.table_area_ota_medium = 'ota_area_medium'
        self.table_area_ota_small = 'ota_area_small'
        self.table_area_ota_detail = 'ota_area_detail'
        self.table_area_ota_point = 'ota_area_point'
        self.config = Configuration.get_config_file()

        self.insert_count = 1
        self.current_ota = ''

    def open_spider(self, spider):
        self.current_ota = spider.name.split('_')[0]

        # initialize db
        self.db = Mysql(self.config, self.db_for)

    def process_item(self, items, spider):
        # Use update or insert function
        for v in items:
            item = items[v]

            if item != {}:
                world_query = 'SELECT id FROM ota_area_world WHERE ota_id=' + item['ota_id']
                world_id = self.db.select(world_query, 'one', self.db_for)

                top_area = self.ota_top_code(self.current_ota, item)
                top_value = {'top_code': top_area['top_code'], 'top_name': top_area['top_name'],
                             'ota_id'  : item['ota_id']}
                if world_id is not None:
                    top_value.update({'world_id': world_id[0]})
                top_unique = ['top_code', 'top_name', 'ota_id']
                top_id = self.db.daily_insert(top_value, self.table_area_ota_top, self.db_for, top_unique)

                large_code = self.booking_large_code(item['large_name'])
                large_value = {'top_id'    : top_id, 'large_code': large_code,
                               'large_name': item['large_name'], 'ota_id': item['ota_id']}
                large_unique = ['large_code', 'large_name', 'ota_id']
                large_id = self.db.daily_insert(large_value, self.table_area_ota_large, self.db_for, large_unique)

                medium_value = {'top_id'     : top_id, 'large_id': large_id, 'medium_code': item['medium_code'],
                                'medium_name': item['medium_name'], 'ota_id': item['ota_id']}
                medium_unique = ['medium_code', 'medium_name', 'ota_id']
                medium_id = self.db.daily_insert(medium_value, self.table_area_ota_medium, self.db_for, medium_unique)

                small_id = 0
                if item.get('small_code') is not None:
                    small_value = {'top_id'    : top_id, 'medium_id': medium_id, 'small_code': item['small_code'],
                                   'small_name': item['small_name'], 'ota_id': item['ota_id']}
                    small_unique = ['small_code', 'small_name', 'ota_id']
                    small_id = self.db.daily_insert(small_value, self.table_area_ota_small, self.db_for, small_unique)

                    if item.get('detail_code') is not None:
                        detail_value = {'top_id'     : top_id, 'small_id': small_id, 'detail_code': item['detail_code'],
                                        'detail_name': item['detail_name'], 'ota_id': item['ota_id']}
                        detail_unique = ['detail_code', 'detail_name', 'ota_id']
                        detail_id = self.db.daily_insert(detail_value, self.table_area_ota_detail, self.db_for,
                                                         detail_unique)

                        if item.get('point_code') is not None:
                            point_value = {'top_id'    : top_id, 'detail_id': detail_id,
                                           'point_code': item['point_code'],
                                           'point_name': item['point_name'], 'ota_id': item['ota_id']}
                            point_unique = ['point_code', 'point_name', 'ota_id']
                            point_id = self.db.daily_insert(point_value, self.table_area_ota_point, self.db_for,
                                                            point_unique)

                # hotel_id from hotel-master
                hotel_id = self.get_hotel_id_via_api(item['ota_hotel_id'])
                value = {'ota_id'   : item['ota_id'], 'ota_hotel_id': item['ota_hotel_id'],
                         'hotel_id' : hotel_id, 'hotel_name': item['hotel_name'],
                         'top_id'   : top_id, 'large_id': large_id,
                         'medium_id': medium_id, 'small_id': small_id,
                         }

                # push data into db
                print(item, value)
                self.db.booking_insert(value, 'ota_area_hotel', self.db_for, self.insert_count)
                self.insert_count += 1
        return items

    def get_hotel_id_via_api(self, ota_sequence_id):
        """
        Getting hotel_id via hotel_master api
        :param ota_sequence_id: seq_id
        :return: hotel_id
        """
        # access via api
        api_url_base = self.config.get('hotel_master_api', 'path')
        api_access_key = self.config.get('hotel_master_api', 'key')
        parameters = {'access_key': api_access_key, 'ota_seq': str(ota_sequence_id), 'ota': self.current_ota}

        hotel_id = 0
        # api request
        url = '{}{}'.format(api_url_base, '/ota_seq_to_master_seq')
        result = requests.get(url, params=parameters)

        # checking api status
        if result.status_code == 200:
            result_dict = json.loads(result.content.decode('utf-8'))

            # checking api response or error in given parameter
            if 'response' in result_dict:
                result_response = result_dict.get('response')
                if len(result_response) > 0:
                    result_response = result_response[0]
                    schema_column = 'repchecker_keyword_mst_seq'
                    # verify field
                    if result_response and schema_column in result_response and result_response[schema_column]:
                        hotel_id = result_response[schema_column]
            else:
                print("[!] Bad Field Error")
                print("Cause: {}".format(result_dict.get('cause')))

        else:
            print("[!] Request Failed: HOTEL-Master API issue.")
            print("[!] Response: {}".format(result))
            hotel_id = None

        return hotel_id

    @staticmethod
    def ota_top_code(ota, item):
        area_top = {'rakuten': {'top_code': 'japan', 'top_name': '日本'},
                    'jalan'  : {'top_code': 'Pj_japan', 'top_name': '日本'},
                    'ikyu'   : {'top_code': 'Pi_japan', 'top_name': '日本'},
                    'ikyubiz': {'top_code': 'Pi_japan', 'top_name': '日本'},
                    'ikyucaz': {'top_code': 'Pi_japan', 'top_name': '日本'},
                    'rurubu' : {'top_code': 'Pru_japan', 'top_name': '日本'},
                    'jtb'    : {'top_code': 'Pjtb_japan', 'top_name': '日本'},
                    'yahoo'  : {'top_code': 'Py_japan', 'top_name': '日本'},
                    'booking': {'top_code': 'jp', 'top_name': '日本'}
                    }

        if ota == 'tripadvisor':
            area_top.update({'tripadvisor': {'top_code': item['top_code'], 'top_name': item['top_name']}})
        if ota == 'agoda':
            area_top.update({'agoda': {'top_code': item['top_code'], 'top_name': item['top_name']}})
        if ota == 'expedia':
            area_top.update({'expedia': {'top_code': item['top_code'], 'top_name': item['top_name']}})

        return area_top[ota]

    @staticmethod
    def booking_large_code(large_name):
        area_large = {'山口県': '4739', '和歌山県': '4734', '徳島県': '4740', '東京都': '4720', '鳥取県': '4735',
         '山形県'       : '4533', '富山県': '4722', '山梨県': '4724', '愛知県': '4142', '埼玉県': '4719', '滋賀県': '4729', '静岡県': '4727',
         '島根県'       : '4736', '栃木県': '4532', '沖縄県': '2351', '大分県': '4652', '岡山県': '4737', '大阪府': '4731', '佐賀県': '4746',
         '長野県'       : '4725', '奈良県': '4733', '新潟県': '4531', '宮城県': '4530', '長崎県': '4653', '神奈川県': '4721',
         '宮崎県'       : '2349', '高知県': '4744', '熊本県': '4647', '鹿児島県': '4648', '京都府': '4730', '三重県': '4140',
         '石川県'       : '2350', '香川県': '4741', '広島県': '4738', '兵庫県': '4732', '茨城県': '4528', '福島県': '4527', '群馬県': '4718',
         '岐阜県'       : '4726', '愛媛県': '4709', '福岡県': '4745', '千葉県': '4525', '福井県': '2348', '岩手県': '4529', '北海道': '2347',
         '青森県'       : '4524', '秋田県': '4523'}

        return area_large[large_name]

