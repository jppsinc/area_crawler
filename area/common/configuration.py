# -*- coding: utf-8 -*-

import os
import sys
import configparser


class Configuration:
    # Config file path
    path_dir = os.path.dirname(__file__)
    configFilePath = '/home/crawler/conf/config.cfg'

    @classmethod
    def get_config_file(cls):
        config = configparser.RawConfigParser()

        try:
            with open(Configuration.configFilePath) as f:
                config.read_file(f)
                return config
        except:
            print("Config file Don't exist.")
            sys.exit()
