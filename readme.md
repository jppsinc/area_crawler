# Area Crawlers

A python crawler repository fetching `area` data from various OTA's.

[![Python version support][shield-python]](#)
[![Pypi version support][shield-pypi]](#)
[![Build status][shield-build]](#)
[![Dependencies][shield-dependencies]](#)



[PSINC]: https://paradigmshift.io/jp
[Python]: https://www.python.org/
[Pypi]: https://pypi.python.org/pypi/pip
[Crawler-UI]: http://stg.crawl-ui.psinc.jp:8080
[shield-dependencies]: https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg
[shield-python]: https://img.shields.io/badge/python-v3.5.0-blue.svg
[shield-pypi]: https://img.shields.io/badge/pypi-v9.0.1-blue.svg
[shield-build]: https://img.shields.io/badge/build-passing-brightgreen.svg

## Build a new crawler logic
scrapy genspider <ota-name>_area <domain-name>

## How to execute crawler
1. Go to Web UI and click run job or cron job from the Jobs. <br> 
   Select project `area`  and your choice of crawler and execute the job.
        
     >for `area` go to [Crawler-UI]  
        
2. After you job started, controller will generate target url and push into redis.
3. After pushing into redis, controller send request for parallel execution to spider servers
4. Spider will run the job and save the crawled data into db

      
## Support OTA
1. Rakuten
2. Jalan
3. Ikyu
4. IkyuBiz
5. IkyuCaz
6. TripAdvisor
7. Rurubu
8. JTB
9. Booking
10. Agoda
11. Yahoo

## Structure 
1. Crawler logic is in spiders folder   
2. Define fields in items folder   
3. Use pipelines.py to save data into DB
